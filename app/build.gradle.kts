plugins {
    kotlin("jvm") version "1.4.10"
    application
    id("org.openjfx.javafxplugin") version "0.0.9"
    id("org.beryx.jlink") version "2.23.1"
}

group = "thunes"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    val kotestVersion = "4.3.2"
    val mockkVersion= "1.10.2"

    implementation(kotlin("stdlib"))
    implementation(project(":core"))

    testImplementation(kotlin("test"))
    testImplementation("io.kotest:kotest-runner-junit5:${kotestVersion}")
    testImplementation("io.kotest:kotest-assertions-core:${kotestVersion}")
    testImplementation("io.mockk:mockk:${mockkVersion}")
}

tasks {
    test {
        useJUnitPlatform()
    }
}

javafx {
    version = "11.0.2"
    modules("javafx.graphics", "javafx.controls")
}

application {
    mainModule.set("thunes.app")
    mainClass.set("thunes.app.Main")
}

val linuxJDK = "/home/kawa/.sdkman/candidates/java/current" // System.getProperty("LINUX_JAVA_HOME")
val windowsJDK = "/home/kawa/Downloads/windows_jdk/jdk-11.0.2"// System.getProperty("WINDOWS_JAVA_HOME")
val windowsJavaFXjmods = "/home/kawa/Downloads/windows_javafx-jmods" // System.getProperty("WINDOWS_JAVAFX_JMODS")

val compileKotlin: org.jetbrains.kotlin.gradle.tasks.KotlinCompile by tasks
val compileJava: JavaCompile by tasks
compileJava.destinationDir = compileKotlin.destinationDir

jlink {
    targetPlatform("linux-x86", linuxJDK)
    targetPlatform("windows") {
        setJdkHome(windowsJDK)
        addExtraModulePath(windowsJavaFXjmods)
    }
    launcher {
        name = "thunes"
        noConsole = true
    }
    imageZip.set(project.file("${project.buildDir}/image-zip/thunes-image.zip"))
}