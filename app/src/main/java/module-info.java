module thunes.app {
    requires kotlin.stdlib;
    requires javafx.controls;
    requires javafx.graphics;
    
    exports thunes.app;
}