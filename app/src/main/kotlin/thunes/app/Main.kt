package thunes.app

import javafx.application.Application
import javafx.beans.property.ReadOnlyObjectWrapper
import javafx.beans.property.ReadOnlyStringWrapper
import javafx.beans.property.SimpleDoubleProperty
import javafx.beans.property.SimpleStringProperty
import javafx.collections.FXCollections
import javafx.scene.Scene
import javafx.scene.control.*
import javafx.scene.layout.BorderPane
import javafx.stage.Stage
import javafx.util.Callback

fun main(args: Array<String>) {
    Application.launch(Main::class.java, *args)
}

class Main : Application() {

    override fun start(primaryStage: Stage) {

        val menuBar = MenuBar(
            Menu("File", null, MenuItem("Open"), MenuItem("Save"), MenuItem("Quit")),
            Menu("Tags" ),
            Menu("Charts"),
            Menu("Preview")
        )

        val year2019 = TreeItem("2019")
        year2019.children.addAll(
            TreeItem("01/2019"),
            TreeItem("02/2019"),
            TreeItem("03/2019"),
            TreeItem("04/2019"),
            TreeItem("05/2019"),
            TreeItem("06/2019"),
        )

        val year2020 = TreeItem("2020")
        year2020.children.addAll(
            TreeItem("01/2020"),
            TreeItem("02/2020"),
            TreeItem("03/2020"),
            TreeItem("04/2020"),
            TreeItem("05/2020"),
            TreeItem("06/2020"),
        )

        val root = TreeItem("My Budget")
        root.isExpanded = true
        root.children.addAll(year2019, year2020)

        val monthList = TreeView(root)

        val operationsTable = TableView(
            FXCollections.observableArrayList(
                OperationModel("2020-01-29", "SFR", -22.0, "charges"),
                OperationModel("2020-01-22", "Auchan", -150.48, "courses"),
                OperationModel("2020-01-17", "Salaire", 2864.30, "salaires"),
                OperationModel("2020-01-11", "Pret", -1235.25, "remboursements"),
                OperationModel("2020-01-10", "Esso", -57.50, "transport"),
                OperationModel("2020-01-02", "SFR", -10.0, "charges"),
            )
        )
        val dateColumn = TableColumn<OperationModel, String>("Date")
        dateColumn.cellValueFactory = Callback { param -> ReadOnlyStringWrapper(param.value.date.get()) }

        val labelColumn = TableColumn<OperationModel, String>("Label")
        labelColumn.cellValueFactory = Callback { param -> ReadOnlyStringWrapper(param.value.label.get()) }

        val amountColumn = TableColumn<OperationModel, Double>("Amount")
        amountColumn.cellValueFactory = Callback { param -> ReadOnlyObjectWrapper(param.value.amount.get()) }

        val tagColumn = TableColumn<OperationModel, String>("Tag")
        tagColumn.cellValueFactory = Callback { param -> ReadOnlyStringWrapper(param.value.tag.get()) }
        tagColumn.isEditable = true

        operationsTable.columns.addAll(dateColumn, labelColumn, amountColumn, tagColumn)

        val scene = Scene(BorderPane(operationsTable, menuBar, null, null, monthList), 640.0, 480.0)

        primaryStage.title = "Thunes"
        primaryStage.scene = scene
        primaryStage.show()
    }
}

data class OperationModel(
    val date: SimpleStringProperty,
    val label: SimpleStringProperty,
    val amount: SimpleDoubleProperty,
    val tag: SimpleStringProperty,
) {
    constructor(date: String, label: String, amount: Double, tag: String) : this(
        SimpleStringProperty(date),
        SimpleStringProperty(label),
        SimpleDoubleProperty(amount),
        SimpleStringProperty(tag)
    )
}
