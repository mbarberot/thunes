plugins {
    kotlin("jvm") version "1.4.10"
}

group = "thunes"
version = "1.0-SNAPSHOT"


repositories {
    mavenCentral()
}

dependencies {
    val spekVersion = "2.0.9"
    val mockkVersion= "1.10.2"

    implementation(kotlin("stdlib"))

    testImplementation(kotlin("test"))
    testImplementation("org.spekframework.spek2:spek-dsl-jvm:$spekVersion")
    testImplementation("io.mockk:mockk:${mockkVersion}")

    testRuntimeOnly("org.spekframework.spek2:spek-runner-junit5:$spekVersion")
    testRuntimeOnly(kotlin("reflect"))
}

tasks {
    test {
        useJUnitPlatform {
            includeEngines("spek2")
        }
    }
}
