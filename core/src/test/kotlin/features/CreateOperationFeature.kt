package features

import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.gherkin.Feature
import thunes.CreateOperation
import thunes.Operation
import thunes.OperationRepository

object CreateOperationFeature : Spek({
    Feature("create an operation") {

        Scenario("create a valid operation") {
            lateinit var operation: Operation
            val operationRepository = mockk<OperationRepository>()
            every { operationRepository.save(any()) } returns Unit

            Given("a valid operation") {
                operation = Operation()
            }

            When("I create this operation") {
                CreateOperation(operationRepository).create(operation)
            }

            Then("This operation is saved") {
                verify { operationRepository.save(operation) }
            }
        }
    }
})

