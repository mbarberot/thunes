package thunes

interface OperationRepository {
    fun save(operation: Operation)
}