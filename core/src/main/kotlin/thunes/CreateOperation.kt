package thunes

class CreateOperation(val repository: OperationRepository) {
    fun create(operation: Operation) {
        repository.save(operation)
    }
}